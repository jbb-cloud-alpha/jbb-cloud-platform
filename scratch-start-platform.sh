#!/bin/sh
git submodule foreach 'git fetch origin; git checkout -f $(git describe --tags `git rev-list --tags --max-count=1`);';
chmod +x ./mvnw;
./mvnw clean install -Papps -DskipTests;
export DOCKER_IP=$(docker-machine ip);
echo "Docker IP is: $DOCKER_IP";
docker-compose build;
docker-compose up -d
